<?php

/**
 * Return a sha1 password
 * @param $pseudo
 * @param $password
 * @return string
 */
function hash_password($pseudo, $password) {
    $password = sha1(strtolower($pseudo).$password."IlikePatatoes!");
    return $password;
}

function slug($string) {
    $data = preg_replace("/[^a-zA-Z0-9\/_|+ -]/", '', $string);
    $data = strtolower(trim($data, '-'));
    $data = preg_replace("/[\/_|+ -]+/", '-', $data);
    return $data;
}

// Suppression des antislashes d'une chaine de caractère
function htmlConvert($string) {
    $string = htmlspecialchars($string);
    return $string;
}
// Suppression des antislashes d'une chaine de caractère
function htmlUnconvert($string) {
    $string = htmlspecialchars_decode($string);
    return $string;
}

