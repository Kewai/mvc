<?php

// cookies
define('DOMAIN', 'local.dev');

// Default style
define('FORM_CLASS', 'form-control');
define('FORM_INPUT', 'input');

// Setting users
define('NAME_MIN_LENGTH', 2);
define('NAME_MAX_LENGTH', 25);
define('PASSWORD_MIN_LENGTH', 7);
define('PASSWORD_MAX_LENGTH', 45);

// Tables
define('TABLE_ARTICLES', 'articles');
define('TABLE_COMMENTS', 'comments');
define('TABLE_FORUMS', 'forums');
define('TABLE_POLLS', 'polls');
define('TABLE_POLLS_VOTES', 'polls_votes');
define('TABLE_POSTS', 'posts');
define('TABLE_REPERTORIES', 'repertory');
define('TABLE_TOPICS', 'topics');
define('TABLE_USERS', 'users');
