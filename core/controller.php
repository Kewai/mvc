<?php

class Controller {

    public function __construct() {

        $this->tpl = $this->model('RainTPL');
        raintpl::configure("tpl_dir", CORE.DS."view/" );
        raintpl::configure("debug", false );

        $this->db = MysqliDb::getInstance();
    }

    public function Model($model, $options = '') {

        require CORE . DS . 'model/' . $model . '.php';
        return new $model($options);
    }

    public function Table($table) {
        require CORE . DS . 'model/table/' . $table . '.php';
        return new $table();
    }

}