<?php

class Error extends Controller {

    public function e404($message) {

        header("HTTP/1.0 404 Not Found");

        $this->tpl->assign('message', $message);
        $this->tpl->draw('error');
    }

}