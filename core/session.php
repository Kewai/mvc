<?php

class Session {

    /*
     * Starting session() everywhere !
     */
    public function __construct() {

        session_start();

        if(isset($_COOKIE['auth']) && !isset($_SESSION['logged'])) {
            $this->db = MysqliDb::getInstance();
            return Self::takeMyCookie();
        }
    }


    // Take my cookie, it's healthy!
    public function takeMyCookie() {

        $auth =  explode('9hj06w8q62z98mf03p79', $_COOKIE['ImAFish']);

        $this->db->where('name', $auth[0]);
        $results = $this->db->getOne('users');
        
        $key = hash_password($results['email'], $results['password']);

        if($key == $auth[1]) {

            $array = array (
                'id'            =>      $results['id'],
                'name'          =>      $results['name'],
                'type'          =>      $results['type'],
                'level'         =>      $results['level'],
                'timezone'      =>      $results['timezone']

                );

            $this->createlogged($array);

            setcookie('ImAFish', $results['name'] . '9hj06w8q62z98mf03p79' . $key, time() + 3600 * 24 * 30, '/', DOMAIN, false, true);
            
        } else {

            setcookie('ImAFish', '', time() - 3600 * 24 * 30, '/', DOMAIN, false, true);
        }
    }

    public static function createlogged($data = []) {

        foreach ($data as $key => $value) {

            $_SESSION['logged'][$key] = $value;
        }
    }

    public static function setFlash($message) {

        $_SESSION['flash'] = $message;
    }

    public static function setError($message, $name) {

        $_SESSION['errors'][$name] = $message;
    }

    public static function showError() {

        if(isset($_SESSION['errors'])) {
            
            $arr =  $_SESSION['errors'];
            unset($_SESSION['errors']);
            return $arr;
        }
    }

    public function flash() {

        if(isset($_SESSION['flash'])) {

            echo $_SESSION['flash'];
            unset($_SESSION['flash']);
        }
    }

    /**
     * Bye-bye ! :'(
     */
    public static function logOut() {

        session_destroy();
        setcookie('ImAFish', '', time() - 3600, '/', DOMAIN, false, true);

    }
} 