<?php

class App {

	private $router;
	protected $request = [];
    protected $controller;
    protected $method;
    protected $fileController;
	protected $params = [];

	public function __construct($router) {

		$this->router = $router;
	}

    /**
     * @param $method
     * @param $alias
     * @param $controller
     * @param $title
     * @return mixed
     */
    public function map($method, $alias, $controller, $title) {

		return $this->router->map($method, $alias, $controller, $title);
	}

    /**
     * @param $match
     */
    public function build($match) {

		// Isolation of Controller#Method
		$request = explode('#', $match['target']);

		// Set Controller, Method default and params
		$this->controller = $request[0];
		$this->method = isset($request[1]) ? $request[1] : 'index';
		$this->params = $match['params'];

		// Include the controller
		require_once CORE . DS . 'controller/' . $this->controller . '.php';

		// Extract file name from $this->controller
		$name = explode('/', $this->controller);
		$this->fileController = array_pop($name);

		// Call the class -> method  with params
		if(method_exists($this->fileController, $this->method)) {
            call_user_func_array([new $this->fileController, $this->method], $this->params);
		} else {
            $error = new Error();
			$message = 'La methode ' . $this->method . ' pour le controller ' . $this->controller . ' est introuvable';
            $error->e404($message);
		}
	}
}
