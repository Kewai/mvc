{include="header"}
<h1>Erreur</h1>

<div class="row">
    <div class="col-l-12 ">
        <div class="panel round red">
            <h1>Une erreur est survenue</h1>
            <p>{$message}</p>
        </div>
    </div>
</div>
{include="footer"}