<!DOCTYPE html>
<html>
<head lang="en">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <meta charset="UTF-8">
    <title>Community</title>
    <link href="/community/css/style.css" rel="stylesheet" />
    <link href="http://fonts.googleapis.com/css?family=Roboto:100italic,100,300italic,300,400italic,400,500italic,500,700italic,700,900italic,900" rel="stylesheet" type="text/css">
</head>
<body>

<div class="site-container">
    <div class="site-pusher">
        <header class="header">
            <a href="#" class="header__icon" id="header__icon"></a>
            <a href="/community" class="header__logo"> <span class="glyphicon glyphicon-home"></span>   Community</a>
            <a href="#" class="header__login" id="menu-toggle"><span class="glyphicon glyphicon-off" aria-hidden="true"></span></a>
            <nav class="menu">
                <a href="/community/mag" alt="">Mag</a>
                <a href="/community/workshop" alt="">Workshop</a>
                <a href="/community/book" alt="">Library</a>
                <a href="/community/forum" alt="">Forum</a>
                <a href="/community/contact" alt="">Ressources</a>
                <a href="/community/custom" alt="">Customize</a>
            </nav>
        </header>
        <div id="wrapper">
            <div id="site-content" class="site-content">
                    {if="isset($_SESSION['logged'])"}
                <div id="sidebar-wrapper">
                    <nav class="sidebar-nav">
                        <a href="/community/me"><span class="glyphicon glyphicon-user btn-lg"></span></a>
                        <a href="#"><span class="glyphicon glyphicon-bell btn-lg"></span></a>
                        <a href="#"><span class="glyphicon glyphicon-comment btn-lg" aria-hidden="true"></a>
                        <a href="#"><span class="glyphicon glyphicon-cloud btn-lg"></span></a>
                        <a href="#"><span class="glyphicon glyphicon-cog btn-lg"></span></a>
                        <a href="#"><span class="glyphicon glyphicon-question-sign btn-lg"></a>
                        <a href="/community/logout"><span class="glyphicon glyphicon-off btn-lg" aria-hidden="true"></a>
                    </nav>
                </div>
                {/if}
                <div class="container">