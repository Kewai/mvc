<?php

// constant path
define('WEBROOT', dirname(__FILE__));
define('ROOT', dirname(WEBROOT));
define('DS', DIRECTORY_SEPARATOR);
define('CORE', ROOT.DS.'mvc');
define('BASE_URL', dirname(dirname($_SERVER['SCRIPT_NAME'])));

// Include file
require CORE . DS . 'core/includes.php';

// Instances
$router = new AltoRouter();
$app = new App($router);

// Set BasePath
$router->setBasePath('mvc');

// map homepage
$app->map( 'GET', '', 'home', 'homepage');
$app->map( 'GET', 'home/', 'home#index', 'home-index');
$app->map( 'GET', 'home/[i:id]', 'admin/home#contact', 'home');

// Match the route
$match = $router->match();

if ($match) {
	$app->build($match);
} else {
	$error = new Error();
    $message = 'La page est introuvable.';
    $error->e404($message);
}
